import numpy as np
import bpy
import os
import sys, importlib

bpy.ops.script.reload()
dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)


from santoriniValid import SantoriniValid

def reload_class(c):
    mod = sys.modules.get(c.__module__)
    importlib.reload(mod)
    return mod.__dict__[c.__name__]

SantoriniValid = reload_class(SantoriniValid)
validate = SantoriniValid()


# check for player movement to out of bound
# check for player building to out of bound
# check for player movement onto another player
# check for player building onto another player
# check for player movement upward to a platform that is too high 0 -> 2, 1->3, 0->3
# check for player movement downard to a platform that is too low 2 -> 0, 
# check for player building onto something that is greater than 4


class SantoriniValidTest:
    
    def generate_board(self):
        np.zeros((6,5,5))
        
        
        return None
    
    # need to pass player_location, board_state, which_player
    def call_check_actions_1(self):
        print('4 valid moves should be available')
        board_state = np.zeros((6,5,5))        
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        board_state[1,1,1] = 1
        board_state[1,1,2] = 1
        board_state[2,3,3] = 1
        board_state[2,3,4] = 1
        board_state[3,3,3] = 1
        board_state[2,0,0] = 1
        board_state[2,0,2] = 1
        board_state[3,0,2] = 1
        
        board_state[2,2,0] = 1
        board_state[3,2,0] = 1
        board_state[4,2,0] = 1
        board_state[5,2,0] = 1
        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)
    
    def call_check_actions_2(self):
        print('player piece should be surrounded by enemy player piece and building blocks. 0 valid moves available')
        
        board_state = np.zeros((6,5,5))
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        board_state[1,1,0] = 1
        board_state[1,1,1] = 1
        
        board_state[2,0,2] = 1
        board_state[3,0,2] = 1
        board_state[2,1,2] = 1
        board_state[3,1,2] = 1
        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)    
    
    def call_check_actions_3(self):
        print('player piece should be surrounded by all building blocks. 0 valid moves available')
        board_state = np.zeros((6,5,5))
        
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        
        board_state[1,4,4] = 1
        board_state[1,3,3] = 1
        
        board_state[2,1,0] = 1
        board_state[3,1,0] = 1
        
        board_state[2,1,1] = 1
        board_state[3,1,1] = 1
        
        board_state[2,1,2] = 1
        board_state[3,1,2] = 1
        
        board_state[2,0,2] = 1
        board_state[3,0,2] = 1
        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)

    def call_check_actions_4(self):
        print('player piece should be surrounded by all building blocks. 1 block is at a level 1 which should allow player to have 5 valid moves')
        
        board_state = np.zeros((6,5,5))
        
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        
        board_state[1,4,4] = 1
        board_state[1,3,3] = 1
        
        board_state[2,1,0] = 1
        board_state[3,1,0] = 1
        
        board_state[2,1,1] = 1
        board_state[3,1,1] = 1
        
        board_state[2,1,2] = 1
        board_state[3,1,2] = 1
        
        board_state[2,0,2] = 1
        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)

    def call_check_actions_5(self):
        print('one of the player pieces should be elevated 2 levels.')
        board_state = np.zeros((6,5,5))
        
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        
        board_state[1,4,4] = 1
        board_state[1,3,3] = 1
        
        board_state[2,0,0] = 1
        board_state[3,0,0] = 1
        

        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)

    def call_check_actions_6(self):
        print('both players are elevated to level 2. 0 available moves')
        board_state = np.zeros((6,5,5))
        
        board_state[0,0,0] = 1
        board_state[0,0,1] = 1
        
        board_state[1,4,4] = 1
        board_state[1,3,3] = 1
        
        board_state[2,0,0] = 1
        board_state[3,0,0] = 1
        
        board_state[2,0,1] = 1
        board_state[3,0,1] = 1
        

        
        new_action_state, valid_moves_remaining, player_state, building_state = validate.check_actions_valid(board_state)
        
        print('checking correctness')
        print(player_state)
        print(building_state)
        print(valid_moves_remaining)

test_validate = SantoriniValidTest()
#test_validate.call_check_actions_1()
#test_validate.call_check_actions_2()
#test_validate.call_check_actions_3()
#test_validate.call_check_actions_4()
#test_validate.call_check_actions_5()
test_validate.call_check_actions_6()