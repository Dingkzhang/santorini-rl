import bpy
import os



class SantoriniSetup:
    
    def setup(self):
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.object.delete(use_global=False)    
        print(os.path.dirname(__file__))
        path = "C:/Users/Dingie/Desktop/ML Projects Revised/New Start/2020/2020.05/2020.05.17 Santorini/Santorini_Blender"
        bpy.ops.wm.append(
            filepath='board.blend',
            directory=path+"/board.blend\\Object\\",
            filename="Plane")
            
        bpy.ops.wm.append(
            filepath='board.blend',
            directory=path+"/board.blend\\Object\\",
            filename="Spacing")
            
        bpy.ops.wm.append(
            filepath='level_1.blend',
            directory=path+"/level_1.blend\\Object\\",
            filename="level_1")
            
        bpy.ops.transform.translate(value=(16, -10, 0.5), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)

        bpy.ops.wm.append(
            filepath='level_2.blend',
            directory=path+"/level_2.blend\\Object\\",
            filename="level_2")

        bpy.ops.transform.translate(value=(16, -10, 1.5), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)
            
        bpy.ops.wm.append(
            filepath='level_3.blend',
            directory=path+"/level_3.blend\\Object\\",
            filename="level_3")

        bpy.ops.transform.translate(value=(16, -10, 2.5), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)

        bpy.ops.wm.append(
            filepath='level_4.blend',
            directory=path+"/level_4.blend\\Object\\",
            filename="level_4")

        bpy.ops.transform.translate(value=(16, -10, 3.5), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)
            
        bpy.ops.wm.append(
            filepath='player_1.blend',
            directory=path+"/player_1.blend\\Object\\",
            filename="player_1")

        bpy.ops.transform.translate(value=(16, 10, 1), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)

        for obj in bpy.context.scene.objects:
            if obj.name.lower().startswith("player_1"):
                obj.name="player_1_a"
            
        bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, 
            TRANSFORM_OT_translate={"value":(0, -4, 0),
            "orient_type":'GLOBAL', "orient_matrix":((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            "orient_matrix_type":'GLOBAL', "constraint_axis":(False, False, False), 
            "mirror":True, "use_proportional_edit":False, 
            "proportional_edit_falloff":'SMOOTH', "proportional_size":1, 
            "use_proportional_connected":False, "use_proportional_projected":False, 
            "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, 
            "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, 
            "remove_on_cancel":False, "release_confirm":False, "use_accurate":False})

        for obj in bpy.context.scene.objects:
            if obj.name.lower().startswith("player_1_a.001"):
                obj.name="player_1_b"
                

        bpy.ops.wm.append(
            filepath='player_2.blend',
            directory=path+"/player_2.blend\\Object\\",
            filename="player_2")

        bpy.ops.transform.translate(value=(16, 2, 1), 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(True, False, False), 
            mirror=True, 
            use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', 
            proportional_size=1, 
            use_proportional_connected=False, use_proportional_projected=False)

        for obj in bpy.context.scene.objects:
            if obj.name.lower().startswith("player_2"):
                obj.name="player_2_a"
            
        bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, 
            TRANSFORM_OT_translate={"value":(0, -4, 0),
            "orient_type":'GLOBAL', "orient_matrix":((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            "orient_matrix_type":'GLOBAL', "constraint_axis":(False, False, False), 
            "mirror":True, "use_proportional_edit":False, 
            "proportional_edit_falloff":'SMOOTH', "proportional_size":1, 
            "use_proportional_connected":False, "use_proportional_projected":False, 
            "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, 
            "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, 
            "remove_on_cancel":False, "release_confirm":False, "use_accurate":False})

        for obj in bpy.context.scene.objects:
            if obj.name.lower().startswith("player_2_a.001"):
                obj.name="player_2_b"
