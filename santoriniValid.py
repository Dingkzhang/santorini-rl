

import bpy
import numpy as np
import sys
# check for if player is there
# check for player movement to out of bound
# check for player building to out of bound
# check for player movement onto another player
# check for player building onto another player
# check for player movement upward to a platform that is too high 0 -> 2, 1->3, 0->3
# check for player movement downard to a platform that is too low 2 -> 0, 
# check for player building onto something that is greater than 4
np.set_printoptions(threshold=sys.maxsize)

class SantoriniValid:
    
    # first element is y-axis, second element is x axis. y-axis positive direction is down x-axis positive direction is right.
    player_actions = [[-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1], [-1, -1]]
    # return result should be [0,[0,1],[0,1]] player, movement, build
    def check_actions_valid(self, board_state):
        
        #should return the available actions
        
        # 6x5x5
        # 3x64x5x5 with 1s for available and -1 for not available
        # if all -1 then terminate game
        
        building_state = np.sum([board_state[2], board_state[3], board_state[4], board_state[5]], axis=0)
        player_state = np.sum([board_state[0], board_state[1]], axis=0)
        action_state = np.zeros((3,64,5,5))
        #player_level, tower_row (y), tile (x)
        player_piece_location = self.player_location(board_state, building_state)
#        print(player_piece_location)
        
        action_state = self.check_player(player_piece_location, board_state, building_state, action_state)
        valid_moves_remaining, action_state = self.check_movement(player_piece_location, board_state, building_state, player_state, action_state)

        valid_moves_remaining = self.check_build(valid_moves_remaining, player_piece_location, board_state, building_state, player_state, action_state)
        
        new_action_state = self.valid_move_board_state(valid_moves_remaining)
        
        
        return new_action_state, valid_moves_remaining, player_state, building_state
    def player_location(self, board_state, building_state):
        
        num_player_checked = 0
        player_piece_location = []
        for tower_row in range(0,5):
            for tile in range(0,5):
                if (board_state[0][tower_row][tile] == 1):
#                    print('found player')
                    player_level = building_state[tower_row][tile]
                    player_piece_location.append([int(player_level), tower_row, tile])
                    
                    num_player_checked += 1
        
                if (num_player_checked == 2):
                    break
            if (num_player_checked == 2):
                break
        
        return player_piece_location
    
     
    def check_player(self, player_piece_location, board_state, building_state, action_state):
        num_player_checked = 0
        # should be 128 actions remaining
        action_checker = 0

        for player_piece in range(len(player_piece_location)):
#            print(player_piece)
            piece = player_piece_location[player_piece]
#            print(piece)
#            print(piece)
            for actions in range(0,64):
                action_state[piece[0], actions, piece[1], piece[2]] = 1
                action_checker += 1
        if (action_checker == 128):
            where_0 = np.where(action_state == 0)
            action_state[where_0] = -1
                            
#        print("inside check player")
#        print("Checking board state")
#        print(board_state)
#        
#        print("Checking building state")
#        print(building_state)
#        
#        print("checking action state")
#        print(action_state)
#        print("checking action_checker")
#        print(action_checker)

        return action_state
    
    def check_movement(self, player_piece_location, board_state, building_state, player_state, action_state):
        available_move = 0
        valid_moves_remaining = [] 
        for player_piece in player_piece_location:
            move_counter = 0
            valid_action = 0

            for action in self.player_actions:
                new_position = [player_piece[1] + action[0], player_piece[2] + action[1]]
#                print(new_position)
                
                # border checker
                if (-1 in new_position or 5 in new_position):
                    valid_action = -1
                # player checker
                elif (player_state[new_position[0]][new_position[1]] == 1):
                    valid_action =  -1
                # building checker    
                elif(abs(player_piece[0] - building_state[new_position[0]][new_position[1]]) >= 2):
                    valid_action = -1
                else:
                    valid_action = 1
                    available_move += 1
                for direction in range(8):
                    action_state[player_piece[0], move_counter, player_piece[1], player_piece[2]] = valid_action
                    if (valid_action == 1):
                        valid_moves_remaining.append([player_piece[0], move_counter, player_piece[1], player_piece[2]])
                    move_counter += 1
#            print(action_state)
#            print(available_move)
#            
#        print(player_piece_location)
#        print(player_state)
#        print(building_state)
#        print(valid_moves_remaining)
        return valid_moves_remaining, action_state
    
    def check_build(self, valid_moves_remaining, player_piece_location, board_state, building_state, player_state, action_state):
        
#        print('inside check_build')
#        print(player_piece_location)
#        print(board_state)
#        print(building_state)
#        print(player_state)
#        print(valid_moves_remaining)
        invalid_moves = []
        for valid_move in valid_moves_remaining:
#            print(valid_move)
            valid_action = 0
            
#            print(valid_move[1] // 8)
#            print(valid_move[1] % 8)
            move_action_index = valid_move[1] // 8
            build_action_index = valid_move[1] % 8 
            
            new_build_position = [valid_move[2] + self.player_actions[move_action_index][0] + self.player_actions[build_action_index][0], 
                                    valid_move[3] + self.player_actions[move_action_index][1] + self.player_actions[build_action_index][1]]
            

            temp_player_state = player_state
            temp_player_state[valid_move[2]][valid_move[3]] = 0
            temp_player_state[valid_move[2] + self.player_actions[move_action_index][0]][valid_move[3] + self.player_actions[move_action_index][1]] = 1
#            print(temp_player_state)
            
            if (-1 in new_build_position or 5 in new_build_position):
                valid_action = -1
                invalid_moves.append(valid_move)
            elif(temp_player_state[new_build_position[0]][new_build_position[1]] == 1):
                valid_action = -1
                invalid_moves.append(valid_move)
            elif(building_state[new_build_position[0]][new_build_position[1]] > 3):
                valid_action = -1
                invalid_moves.append(valid_move)
                
            temp_player_state[valid_move[2]][valid_move[3]] = 1
            temp_player_state[valid_move[2] + self.player_actions[move_action_index][0]][valid_move[3] + self.player_actions[move_action_index][1]] = 0
        for invalid_move in invalid_moves:
            valid_moves_remaining.remove(invalid_move)
#        print('results')
#        print(valid_moves_remaining)
#        print(player_state)
#        print(building_state)
#        print(action_state)
        return valid_moves_remaining
    def valid_move_board_state(self, valid_moves_remaining):

        
        new_action_state = np.zeros((3,64,5,5))
        for valid_move in valid_moves_remaining:
            new_action_state[valid_move[0],valid_move[1],valid_move[2],valid_move[3]] = 1

#        print(new_action_state)
#        print('valid_move_board_state')
#        print(valid_moves_remaining)
        return new_action_state