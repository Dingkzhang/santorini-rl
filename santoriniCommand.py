import numpy as np
import bpy
class SantoriniCommand:
    
    select_command_names = ['level_1', 'level_2', 'level_3', 'level_4', 
                            'player_1_a','player_1_b', 'player_2_a', 'player_2_b']
                            
    moves_available = [[0,1], [0,-1], [1,0], [-1,0], [1,1], [1,-1], [-1,1], [-1, -1]]

    move_magnitude = 5
    board_height = 26
    board_width = 26
        
    def render_board(self, board_state):
        print("rendering board")

        tower_board = np.sum([board_state[2], board_state[3], board_state[4], board_state[5]], axis=0)
        y_coordinate = 2
        x_coordinate = -2
        # y and x are reversed when inserted into matrix due to how the matrix works
        player_x_coordinate = 0
        player_y_coordinate = 0
        player_1_a_selected = False
        player_2_a_selected = False
        # iterates through entire board to build the blocks
        for tower_row in tower_board:
            for tile in tower_row:
                
                x_location = x_coordinate * self.move_magnitude
                y_location = y_coordinate * self.move_magnitude
                print(x_coordinate * self.move_magnitude, y_coordinate * self.move_magnitude)
                final_building_level = "level_0"
                # iterates through the levels to build
                for level in range(1,int(tile)+1):
                    final_building_level = "level_" + str(level)
                    ob = bpy.context.scene.objects["level_" + str(level)]
                    bpy.ops.object.select_all(action='DESELECT')
                    bpy.context.view_layer.objects.active = ob 
                    ob.select_set(True)
                    
                    
                    bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(-16 + x_location, 10 + y_location, 0), "orient_type":'GLOBAL', "orient_matrix":((1, 0, 0), (0, 1, 0), (0, 0, 1)), "orient_matrix_type":'GLOBAL', "constraint_axis":(False, False, False), "mirror":True, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False, "use_accurate":False})
                    print(level)
                    
                
                # checks for correct z coordinate player position
                player_z_coordinate = 1
                
                if (final_building_level == "level_1"):
                    player_z_coordinate = 2
                elif(final_building_level == "level_2"):
                    player_z_coordinate = 3
                elif(final_building_level == "level_3"):
                    player_z_coordinate = 4
                
                # check to see if player 1 is located on this coordinate
                if (board_state[0][player_y_coordinate][player_x_coordinate] == 1):
                    ob = None
                    if (player_1_a_selected):
                        ob = bpy.context.scene.objects["player_1_b"]
                    else:
                        player_1_a_selected = True
                        ob = bpy.context.scene.objects["player_1_a"]
                    bpy.ops.object.select_all(action='DESELECT')
                    bpy.context.view_layer.objects.active=ob
                    ob.select_set(True)
                    ob.location = (x_location,y_location,player_z_coordinate)
                
                # check to see if player 2 is located on thsi coordinate
                if (board_state[1][player_y_coordinate][player_x_coordinate] == 1):
                    ob = None
                    if (player_2_a_selected):
                        ob = bpy.context.scene.objects["player_2_b"]
                    else:
                        player_2_a_selected = True
                        ob = bpy.context.scene.objects["player_2_a"]
                    bpy.ops.object.select_all(action='DESELECT')
                    bpy.context.view_layer.objects.active=ob
                    ob.select_set(True)
                    ob.location = (x_location,y_location,player_z_coordinate)
                    
                
                player_x_coordinate += 1
                x_coordinate += 1
            y_coordinate -= 1
            x_coordinate = -2
            player_y_coordinate += 1
            player_x_coordinate = 0
            
        print("rendering player")
        return None
    
    def render_move(self):
        return None
    
    def duplicate_block(self, block_name):
        return None
    
    def select_block(self, block_name):
        return None
    
    def move_block(self, block_name):
        return None
    
