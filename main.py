import os
import sys
import bpy
dir = os.path.dirname(bpy.data.filepath)

if not dir in sys.path:
    sys.path.append(dir)
    
    
from setup import SantoriniSetup

temp = SantoriniSetup()

temp.setup()