# -*- coding: utf-8 -*-
"""
Created on Sun May 17 15:04:54 2020

@author: Dingie
"""

import gym
from gym import spaces
import numpy as np
import random as rd

import os
import sys, importlib
import bpy

# this is used to append my local directory as part of the system path so that python can find my local classes
bpy.ops.script.reload()
dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)


from santoriniValid import SantoriniValid
from santoriniSetup import SantoriniSetup
from santoriniCommand import SantoriniCommand

# this function is used to reload the class so I don't have to restart blender everytime >:( fuck you blender!!! jk you are awesome
def reload_class(c):
    mod = sys.modules.get(c.__module__)
    importlib.reload(mod)
    return mod.__dict__[c.__name__]

SantoriniSetup = reload_class(SantoriniSetup)
SantoriniCommand = reload_class(SantoriniCommand)
SantoriniValid = reload_class(SantoriniValid)

setup = SantoriniSetup()
command = SantoriniCommand()
validate = SantoriniValid()

# The board state will be a 6x5x5 board
# will need to do 3-D filtering to reduce and learn the pattern to solve for the problem
class SantoriniEnv(gym.Env):
    
    player_actions = [[-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1], [-1, -1]]
    
    
    metadata = {'render.modes': ['human']}    
    def __init__(self, player1_position, player2_position,p1_init, p2_init):
        super(SantoriniEnv, self).__init__()
        self.init_p1_position = p1_init
        self.init_p2_position = p2_init
        self.player1_position = player1_position
        self.player2_position = player2_position
        self.reset()
        
        self.viewer = None

    def check_valid_moves(self):
        return validate.check_actions_valid(self.board_state)

    # parameter1 = player_turn parameter2 = piece_used, parameter3 = piece_move, parameter4 = building_placement
    def step(self, action):
        board_update_position = []
        # updates position of players
        if action[0] == 0:
            self.board_state[action[0], self.player1_position[action[1]][0], self.player1_position[action[1]][1]] = 0
            self.player1_position[action[1]] = [self.player1_position[action[1]][0] + action[2][0], self.player1_position[action[1]][1] + action[2][1]]
            self.board_state[action[0], self.player1_position[action[1]][0], self.player1_position[action[1]][1]] = 1

            board_update_position = [self.player1_position[action[1]][0] + action[3][0], self.player1_position[action[1]][1] + action[3][1]]

        elif action[0] == 1:
            self.board_state[action[0], self.player2_position[action[1]][0], self.player2_position[action[1]][1]] = 0
            self.player2_position[action[1]] = [self.player2_position[action[1]][0] + action[2][0], self.player2_position[action[1]][1] + action[2][1]]
            self.board_state[action[0], self.player2_position[action[1]][0], self.player2_position[action[1]][1]] = 1
        
            board_update_position = [self.player2_position[action[1]][0] + action[3][0], self.player2_position[action[1]][1] + action[3][1]]
        # updates position of the board
        for i in range(2,6):
            if (self.board_state[i, board_update_position[0], board_update_position[1]] == 0):        
                self.board_state[i, board_update_position[0], board_update_position[1]] = 1
                break
        
        self.print_board_state()
        return self.board_state

    # 4 values:, level, move_command, y_location, x_location
    def step_v2(self,action):
        
        move_index = action[1] // 8
        build_index = action[1] % 8
        
        current_y = action[2]
        current_x = action[3]
        new_y = action[2] + self.player_actions[move_index][0]
        new_x = action[3] + self.player_actions[move_index][1]
        
        build_y = action[2] + self.player_actions[move_index][0] + self.player_actions[build_index][0]
        build_x = action[3] + self.player_actions[move_index][1] + self.player_actions[build_index][1]
        
        
        building_state = np.sum([self.board_state[2], self.board_state[3], self.board_state[4], self.board_state[5]] , axis=0)
        
        current_building_level = building_state[build_y,build_x]
        
        build_level_index = int(current_building_level) + 2
        
        self.board_state[0, current_y, current_x] = 0
        self.board_state[0, new_y, new_x] = 1
        self.board_state[build_level_index, build_y, build_x] = 1
        return self.board_state

    def step_blender(self,action):    
        return None
    
    def reset(self):       
        setup.setup()
         
        self.board_state = np.zeros((6,5,5))        
        self.board_state[0,self.init_p1_position[0][0],self.init_p1_position[0][1]] = 1
        self.board_state[0,self.init_p1_position[1][0],self.init_p1_position[1][1]] = 1
        self.board_state[1,self.init_p2_position[0][0],self.init_p2_position[0][1]] = 1
        self.board_state[1,self.init_p2_position[1][0],self.init_p2_position[1][1]] = 1
        
        self.print_board_state()
        
    def render(self):
        command.render_board(self.board_state)
        return None

    def print_board_state(self):
        print(self.board_state)

    def player_swap(self):
        player_state= np.array(self.board_state[0])
        player_state_2 = np.array(self.board_state[1])
        
        self.board_state[0] = player_state_2
        self.board_state[1] = player_state
        print(self.board_state)
test = SantoriniEnv([[0,0],[0,1]],[[0,2],[0,3]],[[0,0],[0,1]],[[0,2],[0,3]])

test_action= [1,36,0,0]
test.step_v2(test_action)

test.player_swap()
_, valid_moves, _, _ = test.check_valid_moves()

print(valid_moves)

test_action = [0,25,0,2]
test.step_v2(test_action)
test.player_swap()

test.render()
test.print_board_state()

# old way to move
#####################################################
#test_action = [0,1,[1,-1],[1,0]]
#test.step(test_action)
#test_action = [1,1,[0,1],[1,0]]
#test.step(test_action)
#test_action = [0,1,[0,1],[1,-1]]

#test.step(test_action)
#test_action = [0,1,[1,-1],[1,1]]
#test.step(test_action)

#test_action = [0,0,[1,0],[0,1]]
#test.step(test_action)

#test_action = [1,0,[1,-1],[1,1]]

#test.step(test_action)

#test_action = [1,0,[1,0],[0,1]]
#test.step(test_action)

#test_action = [1,0,[1,0],[-1,1]]
#test.step(test_action)

#test_action = [1,0,[-1,1],[1,1]]
#test.step(test_action)

#test_action = [1,0,[1,1],[-1,-1]]
#test.step(test_action)

#test.render()
#test.print_board_state()